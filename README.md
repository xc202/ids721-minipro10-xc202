## Rust Serverless Transformer Endpoint

The purpose of this project is to Dockerize a Hugging Face Rust transformer and deploy it as a container to AWS Lambda. Subsequently, a query endpoint will be implemented to allow users to query the transformer by sending requests. The project will package the transformer into a Docker container using a Dockerfile and Rust code, deploy the container to AWS Lambda, and implement an endpoint functionality allowing users to query the transformer via cURL requests, accompanied by appropriate documentation.

### Code Overview

The Rust code defines a Lambda function that utilizes the llm crate for running inference on a language model. Here's a breakdown of the components:

- Model Loading: The function first loads a language model using the llm::load_dynamic function. It specifies the tokenizer source, model architecture, and model parameters such as memory type, batch size, and threading configuration.

- Model Inference: Upon receiving a request, the function extracts the user query from the query string parameters of the request. It then invokes the model with the user query as the prompt and awaits the model's response. The response is logged and returned as the output message.

- Request Handling: The function is designed to handle HTTP requests. It extracts the user query from the request URL's query string parameters and passes it to the model for inference. The response from the model is formatted as an HTTP response with status code 200 and content type "text/html".

- Main Function: The main function initializes tracing and runs the Lambda function using Tokio's run function.

- Model Selection: Bloom I chose is a Rust-based deep learning language model framework that facilitates the training, inference, and deployment of large language models. It provides efficient tokenization, model architecture support, and dynamic loading of pre-trained models. With Bloom, developers can easily integrate powerful language understanding capabilities into their applications, enabling tasks such as text generation, translation, and sentiment analysis.

Be sure to add the GitHub version of llm and other necessary dependencies in Cargo.toml:
```shell
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
rand = "0.8.5"
openssl = { version = "0.10", features = ["vendored"] }
```

The language model can be found [here](https://huggingface.co/rustformers/bloom-ggml)

### Test Locally

1. Install Cargo Lambda

Ensure you have Cargo Lambda installed. You can install it using Cargo:
```shell
cargo install cargo-lambda
```

2. Compile Lambda Function

Compile your Lambda function using Cargo Lambda:
```shell
cargo lambda build
```

3. Invoke Lambda Locally

You can test your Lambda function locally by invoking it with sample events. For example:
```shell
cargo lambda invoke --event '{"text": "Once upon a time"}'
```

4. Alternatively, use the following code and `curl` to test locally:

```shell
cargo lambda watch
```

![Screenshot 2024-04-14 at 12.18.49 PM.png](screenshots%2FScreenshot%202024-04-14%20at%2012.18.49%20PM.png)

### Docker Deployment

The Dockerfile compiles a Rust Lambda function using the cargo-lambda image and sets up the function to run on ARM64 architecture. It copies the compiled binary and the model file into the final Lambda image, and sets the entrypoint to execute the function.

1. Navigate to `Amazon ECR` under the Amazon console and create a new repository.
2. Retrieve the login password for AWS ECR in the us-east-1 region and uses it to authenticate Docker login to the specified ECR repository.
```shell
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <account-number>.dkr.ecr.us-east-1.amazonaws.com
```
3. Build a Docker image named "tf" using Docker Buildx. Buildx is a Docker CLI plugin that extends the Docker build command to support multiple platforms, making it easier to build images for different architectures.
```shell
docker buildx build -t tf .
```
4. Tag a Docker image with the "latest" tag for pushing to an ECR repository in the us-east-1 region.
```shell
docker tag tf:latest <account-number>.dkr.ecr.us-east-1.amazonaws.com/<respository-name>
```
5. Push a Docker image to an Amazon ECR repository located in the us-east-1 region. <account-number> represents your AWS account number, and <repository-name> is the name of the repository where you want to push the image.
```shell
docker push <account-number>.dkr.ecr.us-east-1.amazonaws.com/<respository-name>
```

![Screenshot 2024-04-14 at 4.59.28 PM.png](screenshots%2FScreenshot%202024-04-14%20at%204.59.28%20PM.png)

### AWS Lambda

Create a new AWS Lambda function based on the pre-uploaded container image. Be sure to choose the right architecture based on your running environment. Use `uname -a` to check it.

![Screenshot 2024-04-14 at 2.18.13 PM.png](screenshots%2FScreenshot%202024-04-14%20at%202.18.13%20PM.png)

Create a function URL, be sure to enable CORS.

![WechatIMG411.png](screenshots%2FWechatIMG411.png)

Due to the relatively long execution time of large models, remember to increase the function's usage capacity and timeout.

![Screenshot 2024-04-14 at 2.20.42 PM.png](screenshots%2FScreenshot%202024-04-14%20at%202.20.42%20PM.png)

### Results

You can use curl to invoke the function URL. Due to the need for model inference, it may take several tens of seconds to wait.
```shell
curl https://2z6pdgge32wox5bicoik6x57be0dehhi.lambda-url.us-east-1.on.aws/\?text=Hello%20World
```

![Screenshot 2024-04-14 at 5.12.48 PM.png](screenshots%2FScreenshot%202024-04-14%20at%205.12.48%20PM.png)

DIY your own text here, using `%20` to replace spaces: 
```shell
curl https://2z6pdgge32wox5bicoik6x57be0dehhi.lambda-url.us-east-1.on.aws/\?text=<your sentence here>
```

![WechatIMG415.png](screenshots%2FWechatIMG415.png)

