use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;
use lambda_http::{run, service_fn, tracing, Body, Error, Request, Response, RequestExt};
use llm::{InferenceSessionConfig, ModelKVMemoryType, ModelParameters};

async fn invoke_model(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    // Define the model's tokenizer and architecture
    let tokenizer = llm::TokenizerSource::Embedded;
    let architecture = llm::ModelArchitecture::Bloom;

    // Set the default and alternative model file paths
    let default_model_path = "/Users/chenxiuyuan/Desktop/Rust/hf_transformer/bloom-1b1-q4_0-ggjt.bin";
    let alternative_model_path = "/app/bloom-1b1-q4_0-ggjt.bin";
    let mut model_file = PathBuf::from(default_model_path);
    // Use the alternative path if the model file does not exist at the default path
    if !model_file.exists() {
        println!("Model file not found at default path. Trying alternative path...");
        model_file = PathBuf::from(alternative_model_path);
    }

    // Load the dynamic model, specifying model parameters and a progress callback function
    let model = llm::load_dynamic(
        Some(architecture),
        &model_file,
        tokenizer,
        ModelParameters{
            prefer_mmap: true,
            context_size: 1024,
            lora_adapters: None,
            use_gpu: false,
            gpu_layers: None,
            rope_overrides: None,
            n_gqa: None,
        },
        llm::load_progress_callback_stdout,
    ).expect("Failed to load model");

    // Start a model session, configuring parameters for the inference process
    let mut session = model.start_session(InferenceSessionConfig{
        memory_k_type: ModelKVMemoryType::Float16,
        memory_v_type: ModelKVMemoryType::Float16,
        n_batch: 2,
        n_threads: 5,
    });
    let mut generated_text = Vec::new();

    let inference = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(10),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                std::io::stdout().flush().unwrap();
                generated_text.push(token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Halt),
        },
    );

    let generated_text_string: String = generated_text.join(" ");

    // Return a `Result` containing the generated text string or an error message
    match inference {
        Ok(_) => Ok(generated_text_string),
        Err(e) => Err(Box::new(e)),
    }
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract the user query from the event's query string parameters.
    // If the query parameter is not present, default to a sample query
    let default_story = "Once upon a time";
    let prefix = event.query_string_parameters_ref().and_then(|params| params.first("text")).unwrap_or(default_story);

    // Invoke the model with the user query and handle any errors.
    let output = invoke_model(prefix.to_string()).await.unwrap_or_else(|e| format!("Model Error: {:?}", e));

    // Log the response from the model.
    // println!("Response from Bloomz model: {:?}", output);

    // Return a response with status code 200, content type "text/html", and the output message as the body.
    // The response will be automatically serialized to the appropriate format by the runtime.
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(output))
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
