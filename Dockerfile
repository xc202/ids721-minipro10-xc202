FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /app
ADD . ./

RUN cargo clean && cargo lambda build --release --arm64

#CMD ["sh", "-c", "while true; do echo 'Container is running'; sleep 3600; done"]

FROM public.ecr.aws/lambda/provided:al2-arm64

WORKDIR /app

COPY --from=builder /app/target/lambda/hf_transformer/bootstrap .
COPY --from=builder /app/bloom-1b1-q4_0-ggjt.bin .

ENTRYPOINT ["/app/bootstrap"]